json.visitor_entries visitor_entries.each do |visitor_entry|
  json.id                visitor_entry.id
  json.checkin_at        visitor_entry.checkin_at.present? ? visitor_entry.checkin_at.strftime("%d/%m/%Y %k:%M") : visitor_entry.checkin_at
  json.checkout_at       visitor_entry.checkout_at.present? ? visitor_entry.checkout_at.strftime("%d/%m/%Y %k:%M") : visitor_entry.checkout_at
  json.visitor_name      visitor_entry.visitor.name
  json.retail_chain_name visitor_entry.visitor.retail_chain.name
  json.notes             visitor_entry.notes
end

json.pagination @pagination