class RetailChain < ApplicationRecord
  has_many :users, dependent: :destroy
  has_many :visitors, dependent: :destroy
  has_many :visitor_entries, dependent: :destroy

  accepts_nested_attributes_for :users, allow_destroy: true
  accepts_nested_attributes_for :visitors, allow_destroy: true
  accepts_nested_attributes_for :visitor_entries, allow_destroy: true


  validates :name, presence: true
  validates :cnpj, presence: true

  searchkick word_start: [:name, :cnpj]

  def search_data
    {
      name: name,
      cnpj: cnpj
    }
  end

  def total_visitors
    self.visitors.count
  end

  def as_json(options={})
    super(options.merge!(methods: :total_visitors))
  end
end
