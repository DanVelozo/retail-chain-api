class Visitor < ApplicationRecord
  belongs_to :retail_chain
  has_many   :visitor_entries, dependent: :destroy

  validates :name, presence: true

  searchkick word_start: [:name]

  #mount_uploader :visitor_avatar, VisitorAvatarUploader

  def search_data
    { name: name }
  end
end
