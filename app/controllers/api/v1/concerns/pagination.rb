module Api::V1::Concerns::Pagination
  extend ActiveSupport::Concern

  def pagination_meta(object)        {
    current_page: object.current_page,
    next_page: object.next_page.present? ? object.next_page : 1,
    prev_page: object.prev_page.present? ? object.next_page : 1,
    total_pages: object.total_pages,
    total_count: object.total_count        }
  end
end