class Api::V1::UsersController < ApplicationController
  include Api::V1::Concerns::Authorization
  include Api::V1::Concerns::Pagination

  before_action :authenticate_user
  before_action :set_user, only: %i[show update destroy]
  before_action :set_retail_chain, except: %i[destroy]
  before_action :check_token_blacklist
  before_action :authorize_as_admin

  def index
    if params[:q].present?
      @users = User.search params[:q]

      render json: { message: 'User not found' } if @users.results.empty?
    else
      @users = User.all
    end

    @users

    @pagination = pagination_meta(Kaminari.paginate_array(@users).page(params[:page] ? params[:page].to_i : 1))
  end

  def show
  end

  def create
    @retail_chain.users.create!(user_params)
    user = User.new(user_params)

    if @retail_chain.save
      render json: {
        message: "User #{@retail_chain.users.last.name } successfully created!",
        id: @retail_chain.users.last.id },
        status: :created
    end
  end

  def update
    if @user.update(user_params)
      render json: {
        message: "User #{@user.name} successfully updated!",
        id: @user.id },
        status: :ok
    end
  end

  def destroy
    @user.destroy
    render json: {
      message: "User #{@user.name} successfully removed!",
      id: @user.id }
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def set_retail_chain
    @retail_chain = RetailChain.find(params[:retail_chain_id])
  end

  def user_params
    params.permit(:name, :username, :role, :password, :password_confirmation)
  end
end
