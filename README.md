# API - Redes de Varejo e Controle de Visitantes

## User Stories

1 - Como Administrador, preciso gerenciar Redes Varejistas para dar entrada em novos Clientes.

2 - Como Administrador, preciso pesquisar Redes Varejistas pelo nome para ter acesso as informações.

3 - Como Administrador, preciso gerenciar visitantes de cada rede para que mais pessoas utilizem o sistema.

4 - Como Administrador, preciso enviar fotos dos visitantes para identificá-los.

5 - Como Administrador, preciso pesquisar visitantes por nome para ter acesso as informações.

6 - Como Usuário da rede, preciso me logar no sistema para fazer operações.

7 - Como Usuário da rede, preciso me deslogar no sistema para impedir que outros se passem por mim.

8 - Como Usuário da rede, preciso registrar entradas e saidas de visitantes.

9 - Como Usuário externo, não autenticado, preciso do número total de redes e seus visitantes.

## Dependências

* Ruby 2.6.3
* Rails 6
* PostgreSQL
* ElasticSearch
* [Postman](https://www.postman.com/downloads/)


Para instalar o Ruby ([RVM](https://rvm.io/) ou [Rbenv](https://github.com/rbenv/rbenv)) e o [PostgreSQL](https://www.postgresql.org/), utilize este [guia de instalação](https://gorails.com/setup).


Para instalar o [Elasticsearch](https://www.elastic.co/elasticsearch/), utilize esses guias abaixo:

* **[Ubuntu](https://linuxize.com/post/how-to-install-elasticsearch-on-ubuntu-18-04/)**
* **[MacOS](https://www.elastic.co/guide/en/elasticsearch/reference/current/brew.html)**

Iniciar o Elasticsearch através dos comandos abaixo:
* `systemctl start elasticsearch` / **Ubuntu**

*  `brew services start elasticsearch` / **MacOS**


## Configurando Projeto

Instalar as dependências através do comando:

* `bundle install`

Criar o banco de dados e `migrations`através do comando:

* `rails db:create db:migrate`

Popular com os dados de teste com o comando:

* `rails db:seed`

O `seed` vai criar 2 usuários de teste:

**Admin:**

```
usernamme: admin
password: 123456
```

**User:**

```
usernamme: user
password: 123456
```

Iniciar servidor através do comando:


```
rails s

```

Endpoint:


```
http://localhost:3000/v1/api

```

Para rodar os testes, execute o comando abaixo na raiz do projeto:

```
rspec

```

O `simplecov` vai gerar um report de cobertura de testes, que pode ser acessado através dos comandos a seguir, no terminal:

`open coverage/index.html` | **MacOS**

`xdg-open coverage/index.html` | **Ubuntu**


## Documentação API

A documentação pode ser acessada através do link abaixo:

[Documentação API](https://documenter.getpostman.com/view/1157404/SzmiWbJy?version=latest)