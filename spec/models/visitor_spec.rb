require "rails_helper"

RSpec.describe Visitor, type: :model do
  let!(:retail_chain) { FactoryBot.create(:retail_chain) }

  describe "Associations" do
    it { should belong_to(:retail_chain) }
  end

  describe "Validations" do
    it { should validate_presence_of(:name) }
  end

  describe "Attributes" do
    it "is valid with a name and and retail_chain" do
      visitor = Visitor.new(
        name: "Test Visitor",
        retail_chain: retail_chain
      )
      expect(visitor).to be_valid
    end

    it "is not valid without a name" do
      visitor = Visitor.new(name: nil)
      expect(visitor).to_not be_valid
    end

    it "is not valid without a retail_chain" do
      visitor = Visitor.new(name: 'Test Visitor', retail_chain: nil)
      expect(visitor).to_not be_valid
    end
  end
end